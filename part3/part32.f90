!Project part 3
!Jingyuan Feng 00811719
!-------------------------------
module syncmodule
	implicit none
	complex(kind=8), parameter :: ii=cmplx(0.0,1.0) !ii = sqrt(-1)
    integer :: ntotal, a !total number of oscillators, 
	real(kind=8) :: c,mu,sigma !coupling coefficient, mean, std
	save
end module syncmodule
!-------------------------------

program sync_mpi
    use mpi
    use syncmodule
    implicit none
    integer :: i1,j1
    integer :: nt !number of time steps, number of oscillators
    real(kind=8) :: dt,pi !time step
    integer :: myid, numprocs, ierr
    real(kind=8), allocatable, dimension(:) :: f0,w,f ! initial condition, frequencies, solution
    real(kind=8), allocatable, dimension(:) :: order !order parameter

    !added variables
    integer, allocatable, dimension(:) :: Nper_proc,disps

! Initialize MPI
    call MPI_INIT(ierr)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, numprocs, ierr)
	
	Print *, "!!!!!!!!!!!! mpi init !!!!!!!!!!! ", numprocs

!gather input
    open(unit=10,file='data.in')
        read(10,*) ntotal
        read(10,*) nt
        read(10,*) dt
        read(10,*) c
        read(10,*) sigma
        read(10,*) a
    close(10)

	!Print *, "!!!!!!!!!!!! gather input !!!!!!!!!!!",  ntotal, nt, dt, c, sigma, a

    allocate(f0(ntotal),f(ntotal),w(ntotal),order(nt))
	

!generate initial condition
    pi = acos(-1.d0)
    call random_number(f0)
    f0 = f0*2.d0*pi


!generate frequencies
    mu = 1.d0       
    call random_normal(ntotal,w)
    w = sigma*w+mu    
    
   !Print *, "!!!!!!!!!!!! before euler mpi !!!!!!!!!!!"
!compute solution
    call euler_mpi(MPI_COMM_WORLD,numprocs,ntotal,0.d0,f0,w,dt,nt,f,order)

	!Print *, "!!!!!!!!!!!! after euler mpi !!!!!!!!!!!"
!output solution (after completion of gather in euler_mpi)
       call MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierr)
       if (myid==0) then
        open(unit=11,file='fmpi.dat')
        do i1=1,ntotal
            write(11,*) f(i1)
        end do
        close(11)
        
        open(unit=12,file='order.dat')
        do i1=1,nt
	    write(12,*) order(i1)
	end do
	close(12)
    end if
    !can be loaded in python with: f=np.loadtxt('theta.dat')
   !Print *, "!!!!!!!!!!!! 12 !!!!!!!!!!!"
    call MPI_FINALIZE(ierr)
end program sync_mpi

subroutine euler_mpi(comm,numprocs,n,t0,y0,w,dt,nt,y,order)
    !explicit Euler method, parallelized with mpi
    !input: 
    !comm: MPI communicator
    !numprocs: total number of processes
    !n: number of oscillators
    !t0: initial time
    !y0: initial phases of oscillators
    !w: array of frequencies, omega_i
    !dt: time step
    !nt: number of time steps
    !output: y, final solution
    !order: order at each time step
    use mpi
    use syncmodule
    implicit none
    integer, intent (in) :: n,nt
    real(kind=8), dimension(n), intent(in) :: y0,w
    real(kind=8), intent(in) :: t0,dt
    real(kind=8), dimension(nt), intent(out) :: order
    real(kind=8), dimension(n), intent(out) :: y
    real(kind=8) :: t, part_sum !partialsum
    integer :: i1,k,istart,iend,Nlocal,sender,receiver
    integer :: comm,myid,ierr,numprocs
    integer, allocatable, dimension(:) :: Nper_proc, disps
    real(kind=8), allocatable, dimension(:) :: Rpart,ylocal,f, wlocal
    integer, dimension(MPI_STATUS_SIZE) :: status
  
    call MPI_COMM_RANK(comm, myid, ierr)
    print *, 'start euler_mpi, myid=',myid

    !set initial conditions
    y = y0
    t = t0

    !generate decomposition and allocate sub-domain variables
    call mpe_decomp1d(size(y),numprocs,myid,istart,iend)
    print *, 'istart,iend,threadID=',istart,iend,myid
	
    Nlocal = iend - istart + 1
    allocate(ylocal(Nlocal), f(Nlocal+2*a), wlocal(Nlocal), Rpart(Nlocal))
 	ylocal = y(istart:iend)
 	wlocal = w(istart:iend)
    !time marching
    do k = 1,nt
    	Print *, a+1, Nlocal+a
        f(a+1:Nlocal+a) = ylocal
        
        if (myid<numprocs-1) then
        	receiver = myid + 1
        else
        	receiver = 0
        end if
        
		if (myid > 0) then
			sender = myid - 1
		else
			sender = numprocs - 1
		end if
		
		call MPI_SEND(f(Nlocal+1:Nlocal+a), a, MPI_DOUBLE_PRECISION, receiver, 0, MPI_COMM_WORLD, ierr)
		call MPI_RECV(f(1:a), a, MPI_DOUBLE_PRECISION, sender, MPI_ANY_TAG, MPI_COMM_WORLD, status, ierr)
		call MPI_BARRIER(MPI_COMM_WORLD, ierr)
		if (myid>0) then
        	receiver = myid - 1
        else
        	receiver = numprocs - 1
        end if
        
		if (myid<numprocs-1) then
			sender = myid + 1
		else
			sender = 0
		end if
		
		call MPI_SEND(f(a+1:2*a), a, MPI_DOUBLE_PRECISION, receiver, 0, MPI_COMM_WORLD, ierr)
		call MPI_RECV(f((Nlocal+a+1):(Nlocal+2*a)), a, MPI_DOUBLE_PRECISION, sender, MPI_ANY_TAG, MPI_COMM_WORLD, status, ierr)
		call MPI_BARRIER(MPI_COMM_WORLD, ierr)
        
        call RHS_mpi(Nlocal,t,wlocal,f,Rpart)!add code here)

        ylocal= ylocal + dt*Rpart !ylocal must be declared and defined, Rpart must be declared, and 
                                  !should be returned by RHS_mpi
		t = t + dt
		
		part_sum = sum(exp(ii*ylocal))
		!Print *, "!!!!!!!!!!!! 6 !!!!!!!!!!!"
		call MPI_REDUCE(part_sum, order(k), 1, MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD, ierr)
		!Print *, "!!!!!!!!!!!! 6.5 !!!!!!!!!!!"
		if (myid == 0) then
			order(k) = 1.d0 / n * abs(order(k))
        end if
	end do
		
	print *, 'before collection',myid, maxval(abs(ylocal))
		
	allocate(Nper_proc(numprocs), disps(numprocs))
	
	!Print *, "!!!!!!!!!!!! 7 !!!!!!!!!!!"
	
	call MPI_GATHER(Nlocal, 1, MPI_INT, Nper_proc, 1, MPI_INT, 0, MPI_COMM_WORLD, ierr)

	!Print *, "!!!!!!!!!!!! 8 !!!!!!!!!!!"

	!compute order, and store on myid==0
	if (myid == 0) then
		disps(1)=0
		do i1 = 2, numprocs
			disps(i1) = disps(i1-1)+Nper_proc(i1-1)
		end do
	end if
	
	!Print *, "!!!!!!!!!!!! 9 !!!!!!!!!!!"
	
	call MPI_GATHERV(ylocal, Nlocal, MPI_DOUBLE_PRECISION, y, Nper_proc, disps, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)

	!Print *, "!!!!!!!!!!!! 10 !!!!!!!!!!!"

    if (myid==0) print *, 'finished',maxval(abs(y))
    
   ! Print *, "!!!!!!!!!!!! 11 !!!!!!!!!!!"

end subroutine euler_mpi
!-------------------------
subroutine RHS_mpi(nn,t,w,f,rhs)
    !called by euler_mpi
    !rhs = df/dt
    use syncmodule
    implicit none
    integer, intent(in) :: nn
    real(kind=8), intent(in) :: t
!dimensions of variables below must be added    
    real(kind=8), dimension(nn), intent(in) :: w
    real(kind=8), dimension(nn+2*a), intent(in) :: f
    real(kind=8), dimension(nn), intent(out) :: rhs
    integer :: i1
!Add code to compute rhs
	!Print *, "!!!!!!!!!!!! 21 !!!!!!!!!!!"
    do i1 = a+1,a+nn
        rhs(i1-a) = w(i1-a) - dble(c*sum(sin(f(i1)-f(i1-a:i1+a))))/dble(nn)
    end do
	!Print *, "!!!!!!!!!!!! 22 !!!!!!!!!!!"

end subroutine RHS_mpi


!--------------------------------------------------------------------
!  (C) 2001 by Argonne National Laboratory.
!      See COPYRIGHT in online MPE documentation.
!  This file contains a routine for producing a decomposition of a 1-d array
!  when given a number of processors.  It may be used in "direct" product
!  decomposition.  The values returned assume a "global" domain in [1:n]
!
subroutine MPE_DECOMP1D( n, numprocs, myid, s, e )
    implicit none
    integer :: n, numprocs, myid, s, e
    integer :: nlocal
    integer :: deficit

    nlocal  = n / numprocs
    s       = myid * nlocal + 1
    deficit = mod(n,numprocs)
    s       = s + min(myid,deficit)
    if (myid .lt. deficit) then
        nlocal = nlocal + 1
    endif
    e = s + nlocal - 1
    if (e .gt. n .or. myid .eq. numprocs-1) e = n

end subroutine MPE_DECOMP1D

!--------------------------------------------------------------------

subroutine random_normal(n,rn)

! Adapted from the following Fortran 77 code
!      ALGORITHM 712, COLLECTED ALGORITHMS FROM ACM.
!      THIS WORK PUBLISHED IN TRANSACTIONS ON MATHEMATICAL SOFTWARE,
!      VOL. 18, NO. 4, DECEMBER, 1992, PP. 434-435.

!  The function random_normal() returns a normally distributed pseudo-random
!  number with zero mean and unit variance.

!  The algorithm uses the ratio of uniforms method of A.J. Kinderman
!  and J.F. Monahan augmented with quadratic bounding curves.

IMPLICIT NONE
integer, intent(in) :: n
real(kind=8), intent(out) :: rn(n)
!     Local variables
integer :: i1
REAL(kind=8)     :: s = 0.449871, t = -0.386595, a = 0.19600, b = 0.25472,           &
            r1 = 0.27597, r2 = 0.27846, u, v, x, y, q

!     Generate P = (u,v) uniform in rectangle enclosing acceptance region
do i1=1,n

DO
  CALL RANDOM_NUMBER(u)
  CALL RANDOM_NUMBER(v)
  v = 1.7156d0 * (v - 0.5d0)

!     Evaluate the quadratic form
  x = u - s
  y = ABS(v) - t
  q = x**2 + y*(a*y - b*x)

!     Accept P if inside inner ellipse
  IF (q < r1) EXIT
!     Reject P if outside outer ellipse
  IF (q > r2) CYCLE
!     Reject P if outside acceptance region
  IF (v**2 < -4.d0*LOG(u)*u**2) EXIT
END DO

!     Return ratio of P's coordinates as the normal deviate
rn(i1) = v/u
end do
RETURN


END subroutine random_normal

!OpenMP is useful for fine-grain approach, such as for loop. While MPI typically take a coarse-grain approach. 
!At beginning of simulation, data and tasks are distributed. Each process works on its own problem and communicating when necessary. 
!At the end, all partial results will be collected. Part 3 is a coarse-grain model. 
!At the beginning, I divided the data into small blocks and distributed them to different processors. 
!The processors will communicate with their neighbours by sending and receiving messages. Thus, part 3 should be more suitable for MPI. """