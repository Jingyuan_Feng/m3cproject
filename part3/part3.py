"""Project, part 3"""
"""Jingyuan Feng 00811719"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from p3 import sync #assumes that fortran module sync has been compiled with f2py as p3.so



def oscillator(Nt,T,N,c,mu=1,s=0.1): 
    """Simulate fully-coupled network of oscillators
    Compute simulation for Nt time steps between 0 and T (inclusive)
    input: N: number of oscillators
           c: coupling coefficient
           mu,sigma: distribution parameters for omega_i
    """
    
    sync.c = c
    #sync.w = np.zeros(N)
    sync.w = np.random.normal(m, s, N)
    y0 = np.random.uniform(0,2*np.pi,N)
    t0 = 0
    dt = 1.0*T/(Nt-1)
    t = np.linspace(0,T,Nt)
    
    y,order = sync.rk4(t0,y0,dt,Nt)
    
    return t,sync.w,y0,y,order
    
    

if __name__ == '__main__':
    n,c,m,s = 101,10.0,1.0,0.1
    Nt,T = 1000,100
    t,omega,theta0,theta,order = oscillator(Nt,T,n,c,mu=1,s=0.1)
