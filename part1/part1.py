"""Project, part 1"""
"""Jingyuan Feng 00811719"""
import numpy as np
import matplotlib.pyplot as plt
import rw #Assumes rwmodule has been compiled with f2py to produce rw.so
from rw import rwmodule as rw

def analyze_rnet(Ntime,m,X0,N0,L,Nt,display):
	"""Input variables:
	Ntime: number of time steps
    	m: number of walks
    	X0: initial node, (node with maximum degree if X0=0)
    	N0,L,Nt: recursive network parameters
    	"""
    
    	X,XM = rw.rwnet(Ntime,m,X0,N0,L,Nt,2)   #isample =2 
 
        F = np.zeros((Ntime+1,N0+Nt)) 
        for t in range(Ntime+1):
            bins = np.zeros((N0+Nt)) 
            bins[0:np.amax(X[t,:])] = np.bincount(X[t,:])[1:] #get rid of the 1st element in bincount as nodes start from 1; max element of X[t,:] may be less than N, need to count the bigger nodes in as 0's
            F[t,:] = (1.0*bins)/m
            
  
        Ntime_arr = np.arange(1,Ntime+1)
        
        max_node = np.zeros(Ntime)
        for i in range(Ntime):
            max_node[i]= np.argmax(F[i,:]) 
            
        
        if display:
            plt.figure()
            plt.plot(Ntime_arr,max_node,'.')
            plt.xlabel('timesteps')
            plt.ylabel(' node with the greatest number of walkers')
            plt.title('node with the greatest number of walkers vs. timestep, Jingyuan Feng')
            plt.savefig('p11.png')
            plt.show()
        
        return F
        
    
def convergence_rnet(Ntime,m,X0,N0,L,Nt,display):
    	"""Input variables:
	Ntime: number of time steps
    	m: number of walks
    	X0: initial node, (node with maximum degree if X0=0)
    	N0,L,Nt: recursive network parameters
    	"""
    	"""Function: to get F_NtimeX0_arr we have to first set isample=1 which will then generate X,XM in every timestep.
    	Then use Ntime'th element in XM to get the corresponding F[Ntime,X0].
    	Trend: as number of walkers increases the fraction of m walkers at node X0 at time Ntime decreases. 
    	Although there is oscillation, the general trend shows convergence towards ~0.03/0.04"""
    	
    	m_arr = np.arange(1,m+1)
    	F_NtimeX0_arr = np.zeros(m)
    	for i in range(1,m+1):
    	   X,XM = rw.rwnet(Ntime,i,X0,N0,L,Nt,1)   #isample =1 
    	   F_NtimeX0_arr[i-1] = XM[Ntime]
    	   
    	if display:
            plt.figure()
            plt.plot(m_arr,F_NtimeX0_arr,'.')
            plt.xlabel('number of walks')
            plt.ylabel('the fraction of the m walkers at node X0 at time Ntime')
            plt.title('Dependence of F[Ntime,X0] on m, Jingyuan Feng')
            plt.savefig('p12.png')
            plt.show()
        
  	
if __name__== '__main__':
#add code here to call functions and generate figures
 Ntime,m,X0,N0,L,Nt = 100000,1000,0,5,2,200
 analyze_rnet(Ntime,m,X0,N0,L,Nt,True)
 convergence_rnet(Ntime,200,X0,N0,L,Nt,True) # m is chosen to be 200
