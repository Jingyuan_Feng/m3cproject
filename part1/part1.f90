!Project part 1
!Jingyuan Feng 00811719
module rwmodule
    use network
    use omp_lib

contains


subroutine rwnet(Ntime,Nm,X0,N0,L,Nt,isample,X,XM)
    !random walks on a recursive network
    !Input variables:
    !Ntime: number of time steps
    !Nm: number of walks
    !X0: initial node, (node with maximum degree if X0=0)
    !N0,L,Nt: recursive network parameters
    !Output: X, m random walks on network each with Ntime steps from initial node, X0
    !XM: fraction of walks at initial node, computed every isample timesteps
	implicit none
        integer, intent(in) :: Ntime,Nm,N0,L,Nt,X0,isample
        integer, dimension(Ntime+1,Nm), intent(out) :: X

	real(kind=8), dimension(Ntime / isample + 1), intent(out) :: XM
	integer :: qmax
	integer, dimension(N0+Nt) :: qnet
	!integer, allocatable, dimension(:) :: qnet
	integer, dimension(N0+L*Nt,2) :: enet
	!integer, allocatable, dimension(:,:) :: enet 
	!real(kind=8), allocatable, dimension(:,:) :: R
	real(kind=8), dimension(Ntime,Nm) :: R
	integer, dimension(size(enet,1)*2) :: alist1
	!integer, allocatable, dimension(:) :: alist1
	integer, dimension(size(qnet)) :: alist2
	!integer, allocatable, dimension(:) :: alist2
	integer :: i1, j1, X00, X1 !X00 is the start node, X1 is next node
	!integer(kind=8) :: ta,tb,clock_rate
	!real(kind=8) :: walltime
        !call system_clock(ta)	
	
	!generate network
	call generate(N0,L,Nt,qmax,qnet,enet)
	call adjacency_list(qnet,enet,alist1,alist2)
	!generate random numbers
	call random_number(R)
	
	!init start node. if X0=0 then start node is the node with max degree 
	if (X0 == 0) then
		X00 = maxloc(qnet,dim=1) 
	else
		X00 = X0
	end if
	
	!random walks initial nodes
	do i1 = 1,Nm
		X(1,i1) = X00
	end do
	
	!main rw function
	do i1 = 1,Nm
		do j1 = 2,Ntime+1
			!Print *, "para ", R(j1-1, i1), qnet(X(j1-1,i1)), alist2(X(j1-1,i1))
			X1 = R(j1-1, i1) * qnet(X(j1-1,i1)) + alist2(X(j1-1,i1))
			!Print *, alist1(X1)
			X(j1,i1) = alist1(X1)
		end do !j1
	end do !i1
	
	XM(1) = 1
	X00 = 2
	do i1 = 1+isample,Ntime+1,isample
		X1 = 0
		do j1 = 1,Nm
			if (X(1,j1) == X(i1,j1)) then
				X1 = X1 + 1	
			end if
		end do
		XM(X00) = 1.0*X1/Nm
		X00 = X00 + 1
	end do
	
	!Print *, "XM original"
	!Print *, XM
	
	!call system_clock(tb,clock_rate)
	!walltime = dble(tb-ta)/100/dble(clock_rate)
        !print *, 'wall time=', walltime
	
	!deallocate(qnet, enet, R)
end subroutine rwnet


subroutine rwnet_omp(Ntime,Nm,X0,N0,L,Nt,isample,numthreads,X,XM)
    !parallelized version of rwnet, parallel regions should
    !use numthreads threads
	implicit none
    integer, intent(in) :: Ntime,Nm,N0,L,Nt,X0,isample,numthreads
    integer, dimension(Ntime+1,Nm), intent(out) :: X
    
    ! TODO set numthreads
	real(kind=8), dimension(Ntime / isample + 1), intent(out) :: XM
	integer :: qmax
	integer, dimension(N0+Nt) :: qnet
	integer, dimension(N0+L*Nt,2) :: enet
	real(kind=8), dimension(Ntime,Nm) :: R
	integer, dimension(size(enet,1)*2) :: alist1
	integer, dimension(size(qnet)) :: alist2
	integer :: i1, j1, X00, X1, threadID !X00 is the start node, X1 is the next node
	!integer(kind=8) :: ta,tb,clock_rate
	!real(kind=8) :: walltime
        !call system_clock(ta)	
        
	!generate network
	call generate(N0,L,Nt,qmax,qnet,enet)
	call adjacency_list(qnet,enet,alist1,alist2)
	!generate random numbers
	call random_number(R)
	
	!init start node. if X0=0 then start node is the node with max degree 
	if (X0 == 0) then
		X00 = maxloc(qnet,dim=1)
	else
		X00 = X0
	end if

	!random walks initial nodes
	do i1 = 1,Nm
		X(1,i1) = X00
	end do
	
!$    call omp_set_num_threads(numthreads)
	!main rw function
!$OMP parallel do private(j1, threadID)
	do i1 = 1,Nm
		do j1 = 2,Ntime+1
			threadID = omp_get_thread_num()
			!Print *, "thread ", threadID
			!Print *, "para ", R(j1-1, i1), qnet(X(j1-1,i1)), alist2(X(j1-1,i1))
			X(j1,i1) = alist1(int(R(j1-1, i1) * qnet(X(j1-1,i1)) + alist2(X(j1-1,i1))))
			!Print *, X(j1,i1)
		end do !j1
	end do !i1
!$OMP end parallel do
	
	
	XM(1) = 1
!$OMP parallel do private(threadID)
	do i1 = 1+isample,Ntime+1,isample
		threadID = omp_get_thread_num()
		!Print *, "outer1 thread", threadID, (i1-1)/isample+1
		XM((i1-1)/isample+1) = 0
		do j1 = 1,Nm
			threadID = omp_get_thread_num()
			!Print *, "inner thread", threadID, (i1-1)/isample+1, j1
			if (X(1,j1) == X(i1,j1)) then
				XM((i1-1)/isample+1) = XM((i1-1)/isample+1) + 1	
			end if
		end do
		threadID = omp_get_thread_num()
		!Print *, "outer2 thread", threadID, (i1-1)/isample+1
		XM((i1-1)/isample+1) = 1.0*XM((i1-1)/isample+1)/Nm
	end do
!$OMP end parallel do
	Print *, "XM omp"
	Print *, XM
	
	!call system_clock(tb,clock_rate)
	!walltime = dble(tb-ta)/100/dble(clock_rate)
        !print *, 'wall time=', walltime
	
end subroutine rwnet_omp


end module rwmodule

