"""Project, part 2"""
"""Jingyuan Feng 00811719"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from n1 import network as net
from p1 import flunet as fn
from time import clock,time


def initialize(N0,L,Nt,pflag):
    """Generate network, and initial conditions
    If pflag is true, construct and return transport matrix
    """
    N = N0 + Nt
    qmax,qnet,enet = net.generate(N0,L,Nt)
    A = net.adjacency_matrix(N,enet)
    qmaxloc = np.argmax(qnet)
    y0 = np.zeros(3*N,)
    y0[0:N] = 1
    y0[qmaxloc] = 0.1
    y0[qmaxloc + N] = 0.05
    y0[qmaxloc + N * 2] = 0.05
    
    if pflag:
    	P = np.zeros((N, N))
    	for j in range(0,N):
    	    P[:,j] = qnet*A[:,j]/float(sum(qnet*A[:,j]))		
    	return y0,qmaxloc,P
    else:
    	return y0,qmaxloc

    
def solveFluNet(T,N0,Nt,Ntime,a,b0,b1,g,k,w,y0,P,numthreads,choose_Y): #added variable:N0,Nt,P,choose_Y,numthreads
    """Simulate network flu model at Ntime equispaced times
    between 0 and T (inclusive). Add additional input variables
    as needed clearly commenting what they are and how they are  
    used
    """
    
    N = N0+Nt
    #timespan
    t  = np.linspace(0,T,Ntime)
    #y0  =  y0.reshape(3*N)
    #add input variables to RHS functions if needed
    if choose_Y == 1:
        #integrate ODE specified by RHS
        def RHSnet(y,t,a,b0,b1,g,k,w):
            """RHS used by odeint to solve Flu model"""
            S = y[0:N]
            E = y[N:2*N]
            C = y[2*N:3*N]
            b = b0 + b1*(1.0+ np.cos(2.0*np.pi*t))
            dS = k*(1-S) - b*C*S+ w*(np.dot(P,S)-S)
            dE = b*C*S - (k+a)*E +w*(np.dot(P,E)-E)
            dC = a*E - (g+k)*C + w*(np.dot(P,C)-C)
         
            dy = np.concatenate((dS,dE,dC))
        
            return dy
        #Add code here and to RHS functions above to simulate network flu model
        Y1 = odeint(RHSnet,y0,t,args=(a,b0,b1,g,k,w))
        return t,Y1[:,0:N],Y1[:,N:2*N],Y1[:,2*N:3*N]

    elif choose_Y == 2:
        def RHSnetF(y,t,a,b0,b1,g,k,w):
            """RHS used by odeint to solve Flu model"
            Calculations carried out by fn.rhs
            rhs(n,y,t,a,b0,b1,g,k,w,dy,P)"""
            dy = fn.rhs(y,t,a,b0,b1,g,k,w,P) 
            return dy
        Y2 = odeint(RHSnetF,y0,t,args=(a,b0,b1,g,k,w))
        return t,Y2[:,0:N],Y2[:,N:2*N],Y2[:,2*N:3*N]
        
    elif choose_Y == 3:    
        def RHSnetFomp(y,t,a,b0,b1,g,k,w,numthreads):
            """RHS used by odeint to solve Flu model
            Calculations carried out by fn.rhs_omp
            """
            dy = fn.rhs_omp(y,t,a,b0,b1,g,k,w,numthreads,P)
            return dy
        Y3 = odeint(RHSnetFomp,y0,t,args=(a,b0,b1,g,k,w,numthreads))
        return t,Y3[:,0:N],Y3[:,N:2*N],Y3[:,2*N:3*N]
  
   


def analyze(N0,L,Nt,T,Ntime,a,b0,b1,g,k,threshold,warray,display=False):
    """analyze influence of omega on: 
    1. maximum contagious fraction across all nodes, Cmax
    2. time to maximum, Tmax
    3. maximum fraction of nodes with C > threshold, Nmax    
    Input: N0,L,Nt: recursive network parameters 
           T,Ntime,a,b0,b1,g,k: input for solveFluNet
           threshold: use to compute  Nmax
           warray: contains values of omega
    """
    y0,qmaxloc,P = initialize(N0,L,Nt,True)
    #warray = np.array((0,0.01,0.1,0.2,0.5,1.0))
    Cmax = np.zeros(len(warray))
    Tmax = np.zeros(len(warray))
    N_arr = np.zeros(Ntime)
    Nmax = np.zeros(len(warray))
    for i in range(len(warray)):
        t,S,E,C = solveFluNet(T,N0,Nt,Ntime,a,b0,b1,g,k,warray[i],y0,P,numthreads,1) 
        Cmax[i] = np.amax(C)
        Tmax[i] = np.argmax(np.max(C, axis=1))
        for j in range(Ntime):
            N_arr[j] = 1.0*len(C[j,:][C[j,:]>threshold])/(N0+Nt)
        Nmax[i] = np.amax(N_arr)
    
    
    if display:
            plt.figure()
            plt.plot(warray,Cmax,'-k')
            plt.xlabel('omega')
            plt.ylabel('maximum contagious population observed on a single node')
            plt.title('Cmax vs w, Jingyuan Feng')
            plt.savefig('p21.png')
            plt.figure()
            plt.plot(warray,Tmax,'-b')
            plt.xlabel('omega')
            plt.ylabel('the time at which Cmax occurs')
            plt.title('Tmax vs w, Jingyuan Feng')
            plt.savefig('p22.png')
            plt.figure()
            plt.plot(warray,Nmax,'-r')
            plt.xlabel('omega')
            plt.ylabel('the maximum fraction of nodes with C > threshold at any time')
            plt.title('Nmax vs w, Jingyuan Feng')
            plt.savefig('p23.png')
            plt.show()  
        
    return Cmax,Tmax,Nmax
    

def visualize(enet,C,threshold):
    """Optional, not for assessment: Create figure showing nodes with C > threshold.
    Use crude network visualization algorithm from homework 3
    to display nodes. Contagious nodes should be red circles, all
    others should be black"""
    return None


def performance(N0,L,T,Ntime,a,b0,b1,g,k,w,numthreads): #added variables: N0,L,T,Ntime,a,b0,b1,g,k,w,numthreads
    """function to analyze performance of python, fortran, and fortran+omp approaches
        Add input variables as needed, add comment clearly describing the functionality
        of this function including figures that are generated and trends shown in figures
        that you have submitted
    """
    """Functionality: first part: for each network size, I generate running times for Python, Fortran, Fortran+OMP(numthreads=2) 
    method respectively by calling solveFluNet and clock();
    second part: compute the speedup gain from from Fortran+OMP relative to Fortran.
    Trend: 1.as network size increases,running times for Python,Fortran, Fotran+OMP all increase. 
    Python code stays efficient while Fortran+ Fortran OMP methods get exponentially inefficient. 
    Run times for Fortran and Fortran OMP are approximately the same throughout.
    2. speedup: for network size<200, speedup<1 as parallelled code does not improve performance much with small problem size.
    as network size increases especially when 200<network size<500, Fotran+OMP code gives better performance.
    3. reason for choosing the network size Nt = np.arange(0,601,21): it shows explicitly the trend, gives less than 1min running time, enough coverage for nodes."""
    Nt = np.arange(0,601,21)
    py_time = np.zeros(len(Nt))
    fortran_time = np.zeros(len(Nt))
    fortran_omp_time = np.zeros(len(Nt))
    speedup = np.zeros(len(Nt))

    for i1 in range(len(Nt)):
        y0,qmaxloc,P = initialize(N0,L,Nt[i1],True)
        
        t1 = clock()
        solveFluNet(T,N0,Nt[i1],Ntime,a,b0,b1,g,k,w,y0,P,numthreads,choose_Y=1)
        t2 = clock()
        py_time[i1] = t2-t1
        
        t3 = clock()
        solveFluNet(T,N0,Nt[i1],Ntime,a,b0,b1,g,k,w,y0,P,numthreads,choose_Y=2)
        t4 = clock()
        fortran_time[i1] = t4-t3 
        
        t5 = clock()
        solveFluNet(T,N0,Nt[i1],Ntime,a,b0,b1,g,k,w,y0,P,numthreads,choose_Y=3)
        t6 = clock()
        fortran_omp_time[i1] = t6-t5 
        
        speedup[i1] = 1.0*fortran_time[i1]/fortran_omp_time[i1]
        
    plt.figure()
    plt.plot(Nt,py_time,'-k')
    plt.plot(Nt,fortran_time,'-r')
    plt.plot(Nt,fortran_omp_time,'m')
    plt.xlabel('The network size N = N0+Nt')
    plt.ylabel('Runtime')
    plt.legend(('Python','Fortran','Fortran+OMP'),loc = 'best')
    plt.title('Nt against runtime for Python,Fortran,Fortran OMP methods,Jingyuan Feng')
    plt.savefig('p24.png')
    
    plt.figure()
    plt.plot(Nt,speedup,'-k')
    plt.axhline(y=1, color='m', linestyle='-') #compare with no speedup, i.e speedup = 1
    plt.xlabel('The network size N = N0+Nt')
    plt.ylabel('Speedup')
    plt.legend(loc = 'best')
    plt.title('Nt against speedup gain from Fortran+OMP relative to Fortran,Jingyuan Feng')
    plt.savefig('p25.png')
    plt.show()
    
    
    
if __name__ == '__main__':            
   a,b0,b1,g,k,w = 45.6,750.0,0.5,73.0,1.0,0.1
   N0,L,Nt,T,Ntime,threshold = 5,2,500,2,100,0.1 
   #numthreads =4
   numthreads =2
   #warray = np.array((0,0.01,0.1,0.2,0.5,1.0))
   #warray = np.array((0,0.01,0.1,0.2,0.5,1.0,1.5,2.0,5.0,10.0))
   warray = np.linspace(0,1,100)
   y0,qmaxloc,P = initialize(N0,L,Nt,True)
   t,S,E,C = solveFluNet(T,N0,Nt,Ntime,a,b0,b1,g,k,w,y0,P,numthreads,choose_Y=1)
   #t,S,E,C = solveFluNet(T,N0,Nt,Ntime,a,b0,b1,g,k,w,y0,P,numthreads,choose_Y=2)
   #t,S,E,C = solveFluNet(T,N0,Nt,Ntime,a,b0,b1,g,k,w,y0,P,numthreads,choose_Y=3)
   analyze(N0,L,Nt,T,Ntime,a,b0,b1,g,k,threshold,warray,True)
   performance(N0,L,T,Ntime,a,b0,b1,g,k,w,numthreads)