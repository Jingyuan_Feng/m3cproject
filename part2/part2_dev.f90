!Project part 2
!Jingyuan Feng 00811719
module flunet
        use omp_lib
	implicit none
        !add variables as needed
	save
	contains

! add parameter P as P matrix input
!                    a,b0,b1,g,k,w,P
subroutine rhs(n,y,t,a,b0,b1,g,k,w,P,dy)
    implicit none
    !Return RHS of network flu model
    !input: 
    !n: total number of nodes
    !y: S,E,C
    !t: time
    !a,b0,b1,g,k,w: model parameters
    !output: dy, RHS
    integer, intent(in) :: n
    real(kind=8), dimension(n*3),intent(in) :: y
    real(kind=8), intent(in) :: t,a,b0,b1,g,k,w
    real(kind=8), dimension(n,n),intent(in) :: P
    real(kind=8), dimension(n*3), intent(out) :: dy

    !added variables 
    real(kind=8) :: sumS,sumE,sumC,b
    integer :: i,j,threadID,numthreads

    b = b0 + b1*(1.0+cos(2.0*4*atan(1.0)*t))
    do i = 1,n
        sumS = 0
    	sumE = 0
    	sumC = 0
        do j = 1,n
    	   sumS = sumS+P(i,j)*y(j)-P(j,i)*y(i)
    	   sumE = sumE+P(i,j)*y(n+j)-P(j,i)*y(n+i)
    	   sumC = sumC+P(i,j)*y(2*n+j)-P(j,i)*y(2*n+i)
    	end do ! j
    	dy(i) = k*(1-y(i))-b*y(2*n+i)*y(i)+w*sumS
    	dy(n+i) = b*y(2*n+i)*y(i)-(k+a)*y(n+i)+w*sumE
    	dy(2*n+i) = a*y(n+i)-(g+k)*y(2*n+i)+w*sumC
    end do ! i
    
end subroutine rhs

subroutine rhs_omp(n,y,t,a,b0,b1,g,k,w,numthreads,P,dy)
    implicit none
    !Return RHS of network flu model, parallelized with OpenMP
    !input: 
    !n: total number of nodes
    !y: S,E,C
    !t: time
    !a,b0,b1,g,k,w: model parameters
    !numthreads: the parallel regions should use numthreads threads
    !output: dy, RHS
    integer, intent(in) :: n
    real(kind=8), dimension(n*3),intent(in) :: y
    real(kind=8), intent(in) :: t,a,b0,b1,g,k,w
    real(kind=8), dimension(n,n),intent(in) :: P
    real(kind=8), dimension(n*3), intent(out) :: dy
    
    !added variables 
    real(kind=8) :: sumS,sumE,sumC,b
    integer :: i,j,threadID,numthreads

    b = b0 + b1*(1.0+cos(2.0*4*atan(1.0)*t))
	
!$    call omp_set_num_threads(numthreads)

!$OMP parallel do private(threadID) reduction(+:sumS,sumE,sumC)

    do i = 1,n
       sumS = 0
       sumE = 0
       sumC = 0
       do j = 1,n
    	!Print *, "thread ", threadID
    	sumS = sumS+P(i,j)*y(j)-P(j,i)*y(i)
    	sumE = sumE+P(i,j)*y(n+j)-P(j,i)*y(n+i)
    	sumC = sumC+P(i,j)*y(2*n+j)-P(j,i)*y(2*n+i)
       end do ! j
       dy(i) = k*(1-y(i))-b*y(2*n+i)*y(i)+w*sumS
       dy(n+i) = b*y(2*n+i)*y(i)-(k+a)*y(n+i)+w*sumE
       dy(2*n+i) = a*y(n+i)-(g+k)*y(2*n+i)+w*sumC
    end do ! i
    
!$OMP end parallel do

end subroutine rhs_omp

end module flunet

