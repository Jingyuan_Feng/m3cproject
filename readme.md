M3C 2016 Project

In Part 1 directory:
network.f90: network module from homework3,4 + adjacency list submodule
part1.f90: rwnet(computing network path X and fraction of nodes at initial node XM) + paralled version
part1.py: analyze_rnet + convergence_rnet
p11.png, p12.png: results from analyze_rnet and convergence_rnet


In Part 2 directory:
part2.f90: rhs+ rhs_omp (computing RHS of a system of odes)
part2.py: initialize, solveFluNet,analyze, performance
network.f90: network module from homework3,4 + adjacency list submodule
p21.png, p22.png, p23.png: graph results from <analyze>
p24.png, p25.png: graph results from <performance>

In Part 3 directory:
part3.py: oscillator (solves the system)
part31.f90: rk4, RHS (introduced in oscillator)
part32.f90: sync_mpi +euler_mpi + RHS_mpi


